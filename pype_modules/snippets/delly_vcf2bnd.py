import os
import shlex
import subprocess
import gzip
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    vcf = argv['--vcf']
    output = os.path.dirname(vcf)
    run_id = delly_name(vcf)
    output = os.path.abspath(output)
    output = os.path.join(output, run_id)
    return({'somatic_highConf_plot_bedpe':
                '%s.somatic.highconf.plot.bedpe' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Filter SVs from DELLY calls'
                                       'using Panel of Normal samples'),
                                 add_help=False)

def delly_vcf2bnd_args(parser, subparsers, argv):
    parser.add_argument('--vcf',  dest='vcf',
                        help='Input BCF/VCF file(s)',
                        required=True)
    return parser.parse_args(argv)


def delly_vcf2bnd(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = delly_vcf2bnd_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['bcftools']))
    module('add', program_string(profile.programs['vcftools']))
    module('add', program_string(profile.programs['bedtools']))
    module('add', program_string(profile.programs['vcflib']))
    module('add', program_string(profile.programs['Delly_sv_PoN_filter']))


    genome_fa = profile.files['genome_fa']

    filter_cmd = ['dellyVcf2BndVcf.sh', args.vcf, genome_fa]
    filter_cmd = shlex.split(' '.join(map(str, filter_cmd)))
    log.log.info('Prepare the pcawg filter sv command line:')
    log.log.info(' '.join(map(str, filter_cmd)))
    filter_proc = subprocess.Popen(filter_cmd)
    out = filter_proc.communicate()[0]
    code = filter_proc.returncode
    info = 'dellyVcf2BndVcf.sh exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate delly_vcf2bnd')


def delly_name(vcf):
    base = os.path.basename(vcf)
    base = base.split('.')
    return base[0]
