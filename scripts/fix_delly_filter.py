#!/usr/bin/env python2.7

import argparse
import os
import re
import shlex
import subprocess
import gzip
import tarfile
import logging
from shutil import copyfile
from pype.modules.profiles import get_profiles
from pype.env_modules import get_module_cmd, program_string
from pype.logger import ExtLog

'''
Required pipeline arguments

  --out_circos_plot OUT_CIRCOS_PLOT
                        Output pdf for the cicos plots, type: str
  --seqz_out SEQZ_OUT   seqz_out, type: str
  --out_cov_plot_high OUT_COV_PLOT_HIGH
                        Output path for the coverage plots high confidence
                        calls, type: str
  --tumor_cov TUMOR_COV
                        Coverage file for the tumor sample, type: str
  --normal_cov NORMAL_COV
                        Coverage file for the normal/control sample, type: str
  --input_vcf INPUT_VCF
                        Delly filtered SV calls to reprocess, type: str
'''

def create_symlinks(ref_dict, profile, log):
    '''
    Practical function to create symbolic links
    to the input files corresponding in the
    profile.files list
    '''
    for key in ref_dict:
        log.log.info('Handle reference file: %s' % key)
        if ref_dict[key]:
            log.log.info('Symlink %s to %s' % (ref_dict[key],
                                               profile.files[key]))
            os.symlink(ref_dict[key], profile.files[key])


def get_members(tar, prefix=''):
    offset = len(prefix)
    for tarinfo in tar.getmembers():
        if tarinfo.name.startswith(prefix):
            tarinfo.name = tarinfo.name[offset:]
            yield tarinfo

def find_file(tar, ending):
    '''
    Basic lookup for file name by looking at the file ending.
    A more robust approach may require using the re module
    '''
    for i in get_members(tar):
        if i.name.endswith(ending):
            return i.name

def extract_files(tar, files, outdir):
    tar.extractall(outdir, members=[m for m in tar.getmembers() if m.name in files])



def check_returncode(process, title, command, log):
    if process.returncode == 0:
        log.log.info('%s with command %s succeeded' % (title, command))
    else:
        log.log.error('%s with command %s failed' % (title, command))
        raise Exception('Error while executing %s' % command)


def setup_ref_files(profile_obj, log):
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile_obj.programs['samtools_1']))
    module('add', program_string(profile_obj.programs['tabix']))
    idx_genome = ['samtools', 'faidx', profile_obj.files['genome_fa']]
    idx_genome2 = ['samtools', 'faidx', profile_obj.files['genome_fa_gz']]
    bgz_genome = ['bgzip', '-c', '-f', profile_obj.files['genome_fa']]

    idx_genome = shlex.split(' '.join(map(str, idx_genome)))
    idx_genome2 = shlex.split(' '.join(map(str, idx_genome2)))
    bgz_genome = shlex.split(' '.join(map(str, bgz_genome)))
    log.log.info('Index %s' % ' '.join(map(str, idx_genome2)))
    idx_genome2_proc = subprocess.Popen(idx_genome2)
    idx_genome2_proc.communicate()[0]
    genome_gz_fai = '%s.fai' % profile_obj.files['genome_fa_gz']
    if os.path.isfile(genome_gz_fai):
        bgzip_ref = False
    else:
        log.log.warning(('Failed to index genome.gz, '
                         'will attempt to recover by '
                         're-compressing with bgzip'))
        bgzip_ref = True
    with open(profile_obj.files['genome_fa'], 'wt') as genome_fa:
        try:
            log.log.info('Unzip %s to %s' % (profile_obj.files['genome_fa_gz'],
                                             profile_obj.files['genome_fa']))
            with gzip.open(profile_obj.files['genome_fa_gz'], 'rt') \
                    as genome_fa_gz:
                for line in genome_fa_gz:
                    genome_fa.write(line)
        except IOError:
            log.log.warning('IOError while unzip %s' %
                            profile_obj.files['genome_fa_gz'])

    log.log.info('Index %s' % ' '.join(map(str, idx_genome)))
    idx_genome_proc = subprocess.Popen(idx_genome)
    idx_genome_proc.communicate()[0]
    check_returncode(idx_genome_proc, 'genome.fa index',
                     ' '.join(map(str, idx_genome)), log)
    if bgzip_ref is True:
        os.unlink(profile_obj.files['genome_fa_gz'])
        with open(profile_obj.files['genome_fa_gz'], 'wt') as genome_fa_gz:
            log.log.info('Bgzip %s to %s' % (
                profile_obj.files['genome_fa'],
                profile_obj.files['genome_fa_gz']))
            bgzip_genome_proc = subprocess.Popen(
                bgz_genome, stdout=genome_fa_gz)
            bgzip_genome_proc.communicate()[0]
            check_returncode(bgzip_genome_proc, 'genome.fa bgzip',
                             ' '.join(map(str, bgz_genome)), log)

        log.log.info('Index %s' % ' '.join(map(str, idx_genome2)))
        idx_genome2_proc = subprocess.Popen(idx_genome2)
        idx_genome2_proc.communicate()[0]
        check_returncode(idx_genome2_proc, 'genome.fa.gz index',
                         ' '.join(map(str, idx_genome2)), log)
    log.log.info('Prepare genome lenght command')
    genome_len_cmd = ['cut', '-f1,2', '%s.fai' %
                      profile_obj.files['genome_fa']]
    genome_len_cmd = shlex.split(' '.join(map(str, genome_len_cmd)))
    genome_len_file = profile_obj.files['genome_len']
    log.log.info('Open in write mode file %s' % genome_len_file)
    with open(genome_len_file, 'wt') as genome_len:
        log.log.info('Dump command %s in file %s' %
                     (' '.join(map(str, genome_len_cmd)), genome_len_file))
        genome_len_proc = subprocess.Popen(genome_len_cmd, stdout=genome_len)
        genome_len_proc.communicate()[0]
        check_returncode(genome_len_proc, 'genome lenght',
                         ' '.join(map(str, genome_len_cmd)), log)

    module('rm', program_string(profile_obj.programs['tabix']))
    module('rm', program_string(profile_obj.programs['samtools_1']))
    module('rm', 'tools', 'ngs')


def prepare_tmp_dirs(tempdir, log, subdirs=['databases', 'data', 'runs']):
    if not os.path.isdir(tempdir):
        os.makedirs(tempdir)
    for subdir in subdirs:
        subdir_tmp = os.path.join('/tmp', subdir)
        subdir = os.path.join(tempdir, subdir)
        if not os.path.isdir(subdir):
            log.log.info('Prepare temporary folder %s' % subdir)
            os.mkdir(subdir)
            if subdir_tmp != subdir:
                if not os.path.exists(subdir_tmp):
                    log.log.info('Symlink temporary folder %s to %s' %
                                 (subdir, subdir_tmp))
                    os.symlink(subdir, subdir_tmp)
                else:
                    log.log.error('The temporary folder %s already exists' %
                                  subdir_tmp)


def main():
    parser = argparse.ArgumentParser(description=('Start the delly workflow '
                                                  'with a bio_pype pipeline.'))
    parser.add_argument('--delly_filter_tar',  dest='delly_tar',
                        help='Tar archive containing the delly filter results',
                        required=True)
    parser.add_argument('--cov_tar',  dest='cov_tar',
                        help='Tar archive containing the cov files',  required=True)
    parser.add_argument('--seqz_tar',  dest='seqz_tar',
                        help='Tar archive containing the seqz file',  required=True)
    parser.add_argument('--reference-gz',  dest='ref_gz',
                        help='Genome reference file',  required=True)
    parser.add_argument('--mem',  dest='mem',
                        help=('Amount of max GB of memory to use. '
                              'Default: autodetect'),
                        type=int, required=False)
    parser.add_argument('--ncpu',  dest='ncpu',
                        help='Number of cpu to use. Default: autodetect',
                        type=int, required=False)
    parser.add_argument('--tmp',  dest='tempdir',
                        help='Set the temporary folder',
                        default='/tmp')

    args = parser.parse_args()

    log_dir = os.path.join(os.getcwd(), 'logs')
    log = ExtLog('run_dockstore', log_dir, level=logging.INFO)
    if args.mem:
        log.log.info(('Append PYPE_MEM=%iG to the '
                      'environment variables') % args.mem)
        os.environ['PYPE_MEM'] = '%iG' % args.mem
    if args.ncpu:
        log.log.info(('Append PYPE_NCPU=%i to the '
                      'environment variables') % args.ncpu)
        os.environ['PYPE_NCPU'] = '%i' % args.ncpu

    try:
        tempdir = os.environ['TEMPDIR']
    except KeyError:
        tempdir = args.tempdir

    run_id1 = os.path.basename(args.cov_tar)[:-11]
    run_id2 = os.path.basename(args.delly_tar)[:-22]
    run_id3 = os.path.basename(args.seqz_tar)[:-16]
    if run_id1 == run_id3 == run_id3:
        run_id = run_id1
    else:
        log.log.error('Run_id from input files doen not match %s, %s, %s' % (
            run_id1, run_id2, run_id3))
        raise Exception('Run_id from input files doen not match %s, %s, %s' % (
            run_id1, run_id2, run_id3))
    log.log.info('Set run-id to %s' % run_id)

    log.log.info('Prepare temporary diirectory structure')
    prepare_tmp_dirs(tempdir, log, ['databases', 'data', 'workdir'])

    output_dir = '/tmp/workdir'
    data_dir = '/tmp/data'
    results_dir = os.getcwd()

    log.log.info('Output results in folder %s' % output_dir)
    log.log.info('Use profile %s' % 'docker')
    profile = get_profiles({})['docker']

    ref_dict = {'genome_fa_gz': args.ref_gz}

    create_symlinks(ref_dict, profile, log)

    setup_ref_files(profile, log)

    # prepare path for processes in the pipeline
    proc_dirs = [os.path.join(output_dir, 'plots'),
                 os.path.join(output_dir, 'plots', 'higConf'),
                 os.path.join(output_dir, 'plots', 'circos')]
    for proc_dir in proc_dirs:
        if not os.path.isdir(proc_dir):
            os.mkdir(proc_dir)

    cov_tar = tarfile.open(args.cov_tar)
    seqz_tar = tarfile.open(args.seqz_tar)
    delly_tar = tarfile.open(args.delly_tar)
    cov_file_nor = find_file(cov_tar, '_normal_10K.gcnorm.cov.gz')
    cov_file_tum = find_file(cov_tar, '_tumor_10K.gcnorm.cov.gz')
    seqz_file = find_file(seqz_tar, '.seqz.gz')
    seqz_tbi_file = find_file(seqz_tar, '.seqz.gz.tbi')
    vcf_file =  find_file(delly_tar, '.svFreq.PoN.somatic.highConf.vcf.gz')
    vcf_tbi_file =  find_file(delly_tar, '.svFreq.PoN.somatic.highConf.vcf.gz.tbi')
    extract_files(cov_tar, [cov_file_nor, cov_file_tum], data_dir)
    extract_files(seqz_tar, [seqz_file, seqz_tbi_file], data_dir)
    extract_files(delly_tar, [vcf_file, vcf_tbi_file], data_dir)


    pype_cmd = ['pype', '--profile', 'docker', 'pipelines',
                '--queue', 'parallel', '--log', log_dir,
                'fix_bnd',
                '--input_vcf', os.path.join(data_dir, vcf_file),
                '--normal_cov', os.path.join(data_dir, cov_file_nor),
                '--tumor_cov', os.path.join(data_dir, cov_file_tum),
                '--out_cov_plot_high', os.path.join(output_dir, 'plots',
                                                    'higConf'),
                '--out_circos_plot', os.path.join(
                    output_dir, 'plots', 'circos',
                    '%s_circos.pdf' % run_id),
                '--seqz_out', os.path.join(data_dir, seqz_file)]

    pype_cmd = shlex.split(' '.join(map(str, pype_cmd)))
    log.log.info('Prepare pype command line:')
    log.log.info(' '.join(map(str, pype_cmd)))
    pype_proc = subprocess.Popen(pype_cmd)
    pype_proc.communicate()[0]

    # Archive and moving the results

    # prepare output paths for the archiving of the results
    res_dirs = [os.path.join(results_dir, 'bndout'),
                os.path.join(results_dir, 'highconfout'),
                os.path.join(results_dir, 'dellyout')]

    for res_dir in res_dirs:
        if not os.path.isdir(res_dir):
            os.mkdir(res_dir)

    bnd_dir = os.path.join(results_dir, 'bndout')
    highconf_dir = os.path.join(results_dir, 'highconfout')

    log.log.info('Copy BND results to %s' % bnd_dir)
    filter_dir = os.path.join(
        data_dir, 'delly_combined', 'filter_calls')
    for result in os.listdir(filter_dir):
        filtered_file = os.path.join(filter_dir, result)
        if 'bric_embl-delly' in result:
            copyfile(filtered_file, os.path.join(bnd_dir, result))

    log.log.info('Copy high conf circos plot to %s' % highconf_dir)
    circos_dir = os.path.join(output_dir, 'plots', 'circos')
    for result in os.listdir(circos_dir):
        plot_file = os.path.join(circos_dir, result)
        copyfile(plot_file, os.path.join(highconf_dir, result))

    log.log.info('Archive log directory %s' % log_dir)
    tar = tarfile.open(
        os.path.join(results_dir, '%s_logs.tar.gz' % run_id), 'w:gz')
    tar.add(log_dir, arcname='logs')
    tar.close()

    log.log.info('Archive plots directory %s' %
                 os.path.join(output_dir, 'plots'))
    cov_plots = tarfile.open(os.path.join(
        results_dir, '%s_plots.tar.gz' % run_id), 'w:gz')
    cov_plots.add(os.path.join(output_dir, 'plots'), arcname='plots')
    cov_plots.close()


if __name__ == '__main__':
    main()
