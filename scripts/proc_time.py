#! /usr/bin/env python2.7

from __future__ import division
import os
import yaml
import argparse


def run_log_dir(path):
    for item in os.listdir(path):
        if item.endswith('_sv_pipeline_docker'):
            return os.path.join(path, item)

def find_delly_proc(pipeline, path):
    delly_procs = dict()
    for key in pipeline:
        cmd = pipeline[key]['command']
        cmd = cmd.strip().split(' ')
        if cmd[6] == 'delly':
            db_file = os.path.join(path, '%s_db.yaml' % key)
            try:
                timing = yaml.safe_load(file(db_file, 'rt'))
            except IOError:
                raise IOError('DB fie %s was not found' % db_file)
            name = 'delly_%s' % cmd[12]
            delly_procs[name] = timing
    return delly_procs

def find_sequenza_proc(pipeline, path):
    sequenza_procs = dict()
    for key in pipeline:
        cmd = pipeline[key]['command']
        cmd = cmd.strip().split(' ')
        if cmd[6] == 'sequenza':
            db_file = os.path.join(path, '%s_db.yaml' % key)
            try:
                timing = yaml.safe_load(file(db_file, 'rt'))
            except IOError:
                raise IOError('DB fie %s was not found' % db_file)
            sequenza_procs['sequenza'] = timing
    return sequenza_procs

def find_seqzmerge_proc(pipeline, path):
    sequenza_procs = dict()
    for key in pipeline:
        cmd = pipeline[key]['command']
        cmd = cmd.strip().split(' ')
        if cmd[6] == 'merge_seqz':
            db_file = os.path.join(path, '%s_db.yaml' % key)
            try:
                timing = yaml.safe_load(file(db_file, 'rt'))
            except IOError:
                raise IOError('DB fie %s was not found' % db_file)
            sequenza_procs['merge_seqz'] = timing
    return sequenza_procs

def find_pon_proc(pipeline, path):
    pon_procs = dict()
    for key in pipeline:
        cmd = pipeline[key]['command']
        cmd = cmd.strip().split(' ')
        if cmd[6] == 'Delly_sv_PoN_filter':
            db_file = os.path.join(path, '%s_db.yaml' % key)
            try:
                timing = yaml.safe_load(file(db_file, 'rt'))
            except IOError:
                raise IOError('DB fie %s was not found' % db_file)
            pon_procs['PoN_filter'] = timing
    return pon_procs

def find_tot_time(pipeline, path):
    time_proc = dict()
    time_min = None
    time_max = None
    for key in pipeline:
        db_file = os.path.join(path, '%s_db.yaml' % key)
        try:
            timing = yaml.safe_load(file(db_file, 'rt'))
        except IOError:
            raise IOError('DB fie %s was not found' % db_file)
        if time_min == None and time_max == None:
            time_min = timing['start']
            time_max = timing['end']
        else:
            if time_min > timing['start']:
                time_min = timing['start']
            if time_max < timing['end']:
                time_max = timing['end']
    tot_timing = {'start': time_min, 'end': time_max}
    time_proc['total'] = tot_timing
    return time_proc

def find_bam2seqz_proc(pipeline, path):
    seqz_times = list()
    for key in pipeline:
        cmd = pipeline[key]['command']
        cmd = cmd.strip().split(' ')
        if cmd[6] == 'bam2seqz':
            db_file = os.path.join(path, '%s_db.yaml' % key)
            try:
                timing = yaml.safe_load(file(db_file, 'rt'))
            except IOError:
                raise IOError('DB fie %s was not found' % db_file)
            seqz_times.append(timing)
    start = seqz_times[0]['start']
    end = seqz_times[0]['end']
    for time in seqz_times:
        if time['start'] < start:
            start = time['start']
        if time['end'] > end:
            end = time['end']
    return {'bam2seqz': {'start': start, 'end': end}}

def find_cov_proc(pipeline, path):
    cov_procs = dict()
    for key in pipeline:
        cmd = pipeline[key]['command']
        cmd = cmd.strip().split(' ')
        if cmd[6] == 'cov_tool':
            db_file = os.path.join(path, '%s_db.yaml' % key)
            try:
                timing = yaml.safe_load(file(db_file, 'rt'))
            except IOError:
                raise IOError('DB fie %s was not found' % db_file)
            if cmd[10] == '1000':
                size = '1k'
            else:
                size = '10k'
            if cmd[12].endswith('tumor.bam'):
                sample = 'tumor'
            else:
                sample = 'normal'
            name = 'cov_%s_%s' % (sample, size)
            cov_procs[name] = timing
    return cov_procs

def time_proc(runid, name, timing):
    diff = timing['end'] - timing['start']
    hours = int(diff.seconds // (60 * 60))
    mins = int((diff.seconds // 60) % 60)
    return('%s.%s' % (runid, name), hours, mins)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', help='Path of logs folder',
                        required=True)
    parser.add_argument('-r', '--run_id', help='Run Id',
                        required=True)
    parser.add_argument('-o', '--out', help='Output file',
                        required=True)
    args = parser.parse_args()

    run_dir = run_log_dir(args.path)
    yaml_run = os.path.join(run_dir, 'pipeline_runtime.yaml')
    yaml_db = os.path.join(run_dir, 'parallel_run', 'yaml_db')
    if not os.path.isdir(yaml_db):
        raise IOError('The folder %s does not exists' % yaml_db)

    try:
        pipeline_procs = yaml.safe_load(file(yaml_run, 'rt'))
    except OSError as e:
        raise e

    timing_procs = find_delly_proc(pipeline_procs, yaml_db)
    cov_proc = find_cov_proc(pipeline_procs, yaml_db)
    timing_procs.update(cov_proc)
    bam2seqz_proc = find_bam2seqz_proc(pipeline_procs, yaml_db)
    timing_procs.update(bam2seqz_proc)
    sequenza_proc = find_sequenza_proc(pipeline_procs, yaml_db)
    timing_procs.update(sequenza_proc)
    mergeseqz_proc = find_seqzmerge_proc(pipeline_procs, yaml_db)
    timing_procs.update(mergeseqz_proc)
    pon_proc = find_pon_proc(pipeline_procs, yaml_db)
    timing_procs.update(pon_proc)
    tot_proc = find_tot_time(pipeline_procs, yaml_db)
    timing_procs.update(tot_proc)
    with open(args.out, 'wt') as out_file:
        out_file.write('%s\t%s\t%s\n' % ('name', 'hours', 'minutes'))
        for proc in timing_procs:
            res = time_proc(args.run_id, proc, timing_procs[proc])
            out_file.write('%s\t%s\t%s\n' % res)


if __name__ == "__main__":
    main()
