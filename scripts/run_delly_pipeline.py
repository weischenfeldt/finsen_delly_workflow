#!/usr/bin/env python2.7

import argparse
import os
import shlex
import subprocess
import gzip
import tarfile
import logging
from shutil import copyfile
from pype.modules.profiles import get_profiles
from pype.env_modules import get_module_cmd, program_string
from pype.logger import ExtLog


def create_symlinks(ref_dict, profile, log):
    '''
    Practical function to create symbolic links
    to the input files corresponding in the
    profile.files list
    '''
    for key in ref_dict:
        log.log.info('Handle reference file: %s' % key)
        if ref_dict[key]:
            log.log.info('Symlink %s to %s' % (ref_dict[key],
                                               profile.files[key]))
            os.symlink(ref_dict[key], profile.files[key])


def get_members(tar, prefix):
    if not prefix.endswith('/'):
        prefix += '/'
    offset = len(prefix)
    for tarinfo in tar.getmembers():
        if tarinfo.name.startswith(prefix):
            tarinfo.name = tarinfo.name[offset:]
            yield tarinfo


def unpack_pon_archive(tar_path, out_path, prefix):
    with open(tar_path, 'r') as tar_file:
        tar = tarfile.open(fileobj=tar_file)
        for element in get_members(tar, prefix):
            tar.extract(element, out_path)


def check_returncode(process, title, command, log):
    if process.returncode == 0:
        log.log.info('%s with command %s succeeded' % (title, command))
    else:
        log.log.error('%s with command %s failed' % (title, command))
        raise Exception('Error while executing %s' % command)


def setup_ref_files(wig, profile_obj, log):
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile_obj.programs['samtools_1']))
    module('add', program_string(profile_obj.programs['tabix']))
    module('add', program_string(profile_obj.programs['sequenza-utils']))
    idx_genome = ['samtools', 'faidx', profile_obj.files['genome_fa']]
    idx_genome2 = ['samtools', 'faidx', profile_obj.files['genome_fa_gz']]
    bgz_genome = ['bgzip', '-c', '-f', profile_obj.files['genome_fa']]

    gc50_wig = ['sequenza-utils', 'gc_wiggle',
                '-f', profile_obj.files['genome_fa'],
                '-o', profile_obj.files['genome_gc_wig'],
                '-w', 50]
    idx_genome = shlex.split(' '.join(map(str, idx_genome)))
    idx_genome2 = shlex.split(' '.join(map(str, idx_genome2)))
    bgz_genome = shlex.split(' '.join(map(str, bgz_genome)))
    gc50_wig = shlex.split(' '.join(map(str, gc50_wig)))
    log.log.info('Index %s' % ' '.join(map(str, idx_genome2)))
    idx_genome2_proc = subprocess.Popen(idx_genome2)
    idx_genome2_proc.communicate()[0]
    genome_gz_fai = '%s.fai' % profile_obj.files['genome_fa_gz']
    if os.path.isfile(genome_gz_fai):
        bgzip_ref = False
    else:
        log.log.warning(('Failed to index genome.gz, '
                         'will attempt to recover by '
                         're-compressing with bgzip'))
        bgzip_ref = True
    with open(profile_obj.files['genome_fa'], 'wt') as genome_fa:
        try:
            log.log.info('Unzip %s to %s' % (profile_obj.files['genome_fa_gz'],
                                             profile_obj.files['genome_fa']))
            with gzip.open(profile_obj.files['genome_fa_gz'], 'rt') \
                    as genome_fa_gz:
                for line in genome_fa_gz:
                    genome_fa.write(line)
        except IOError:
            log.log.warning('IOError while unzip %s' %
                            profile_obj.files['genome_fa_gz'])

    log.log.info('Index %s' % ' '.join(map(str, idx_genome)))
    idx_genome_proc = subprocess.Popen(idx_genome)
    idx_genome_proc.communicate()[0]
    check_returncode(idx_genome_proc, 'genome.fa index',
                     ' '.join(map(str, idx_genome)), log)
    if wig is None:
        log.log.info('GC wig %s' % ' '.join(map(str, gc50_wig)))
        gc50_wig_proc = subprocess.Popen(gc50_wig)
        gc50_wig_proc.communicate()[0]
        check_returncode(gc50_wig_proc, 'GC wiggle',
                         ' '.join(map(str, gc50_wig)), log)
    else:
        log.log.info('Symlink %s to %s' % (
            wig, profile_obj.files['genome_gc_wig']))
        os.symlink(wig, profile_obj.files['genome_gc_wig'])

    if bgzip_ref is True:
        os.unlink(profile_obj.files['genome_fa_gz'])
        with open(profile_obj.files['genome_fa_gz'], 'wt') as genome_fa_gz:
            log.log.info('Bgzip %s to %s' % (
                profile_obj.files['genome_fa'],
                profile_obj.files['genome_fa_gz']))
            bgzip_genome_proc = subprocess.Popen(
                bgz_genome, stdout=genome_fa_gz)
            bgzip_genome_proc.communicate()[0]
            check_returncode(bgzip_genome_proc, 'genome.fa bgzip',
                             ' '.join(map(str, bgz_genome)), log)

        log.log.info('Index %s' % ' '.join(map(str, idx_genome2)))
        idx_genome2_proc = subprocess.Popen(idx_genome2)
        idx_genome2_proc.communicate()[0]
        check_returncode(idx_genome2_proc, 'genome.fa.gz index',
                         ' '.join(map(str, idx_genome2)), log)
    log.log.info('Prepare genome lenght command')
    genome_len_cmd = ['cut', '-f1,2', '%s.fai' %
                      profile_obj.files['genome_fa']]
    genome_len_cmd = shlex.split(' '.join(map(str, genome_len_cmd)))
    genome_len_file = profile_obj.files['genome_len']
    log.log.info('Open in write mode file %s' % genome_len_file)
    with open(genome_len_file, 'wt') as genome_len:
        log.log.info('Dump command %s in file %s' %
                     (' '.join(map(str, genome_len_cmd)), genome_len_file))
        genome_len_proc = subprocess.Popen(genome_len_cmd, stdout=genome_len)
        genome_len_proc.communicate()[0]
        check_returncode(genome_len_proc, 'genome lenght',
                         ' '.join(map(str, genome_len_cmd)), log)

    module('rm', program_string(profile_obj.programs['sequenza-utils']))
    module('rm', program_string(profile_obj.programs['tabix']))
    module('rm', program_string(profile_obj.programs['samtools_1']))
    module('rm', 'tools', 'ngs')


def setup_bams(tumor, normal, tumor_bai, normal_bai,
               bam_path, profile_obj, log):
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile_obj.programs['samtools_1']))
    module('add', program_string(profile_obj.programs['sequenza-utils']))

    tumor_link = os.path.join(bam_path, 'tumor.bam')
    normal_link = os.path.join(bam_path, 'normal.bam')
    tumor_bai_link = os.path.join(bam_path, 'tumor.bam.bai')
    normal_bai_link = os.path.join(bam_path, 'normal.bam.bai')
    log.log.info('Symlink %s to %s' % (tumor, tumor_link))
    log.log.info('Symlink %s to %s' % (normal, normal_link))
    os.symlink(tumor, tumor_link)
    os.symlink(normal, normal_link)
    idx_tumor = ['samtools', 'index', tumor_link]
    idx_normal = ['samtools', 'index', normal_link]

    idx_tumor = shlex.split(' '.join(map(str, idx_tumor)))
    idx_normal = shlex.split(' '.join(map(str, idx_normal)))

    if tumor_bai is None:
        log.log.info('Index %s' % ' '.join(map(str, idx_tumor)))
        idx_tumor_proc = subprocess.Popen(idx_tumor)
        idx_tumor_proc.communicate()[0]
        check_returncode(idx_tumor_proc, 'tumor bam index',
                         ' '.join(map(str, idx_tumor)), log)
    else:
        log.log.info('Symlink %s to %s' % (tumor_bai, tumor_bai_link))
        os.symlink(tumor_bai, tumor_bai_link)
    if normal_bai is None:
        log.log.info('Index %s' % ' '.join(map(str, idx_normal)))
        idx_normal_proc = subprocess.Popen(idx_normal)
        idx_normal_proc.communicate()[0]
        check_returncode(idx_normal_proc, 'normal bam index',
                         ' '.join(map(str, idx_normal)), log)
    else:
        log.log.info('Symlink %s to %s' % (normal_bai, normal_bai_link))
        os.symlink(normal_bai, normal_bai_link)

    # Remove module from main process
    module('rm', program_string(profile_obj.programs['sequenza-utils']))
    module('rm', program_string(profile_obj.programs['samtools_1']))
    module('rm', 'tools', 'ngs')

    return {'tumor': tumor_link, 'normal': normal_link}


def prepare_tmp_dirs(tempdir, log, subdirs=['databases', 'data', 'runs']):
    if not os.path.isdir(tempdir):
        os.makedirs(tempdir)
    for subdir in subdirs:
        subdir_tmp = os.path.join('/tmp', subdir)
        subdir = os.path.join(tempdir, subdir)
        if not os.path.isdir(subdir):
            log.log.info('Prepare temporary folder %s' % subdir)
            os.mkdir(subdir)
            if subdir_tmp != subdir:
                if not os.path.exists(subdir_tmp):
                    log.log.info('Symlink temporary folder %s to %s' %
                                 (subdir, subdir_tmp))
                    os.symlink(subdir, subdir_tmp)
                else:
                    log.log.error('The temporary folder %s already exists' %
                                  subdir_tmp)


def main():
    parser = argparse.ArgumentParser(description=('Start the delly workflow '
                                                  'with a bio_pype pipeline.'))
    parser.add_argument('--run-id',  dest='run_id',
                        help='Sample id, identifier of the run', required=True)
    parser.add_argument('--normal-bam',  dest='normal_bam',
                        help='Normal Bam file',  required=True)
    parser.add_argument('--tumor-bam',  dest='tumor_bam',
                        help='Tumor Bam file',  required=True)
    parser.add_argument('--normal-bam-index',  dest='normal_bai',
                        help='Normal Bam file',  required=False)
    parser.add_argument('--tumor-bam-index',  dest='tumor_bai',
                        help='Tumor Bam file',  required=False)
    parser.add_argument('--reference-gz',  dest='ref_gz',
                        help='Genome reference file',  required=True)
    parser.add_argument('--reference-gc',  dest='ref_gc',
                        help='Genome reference GC content file',
                        required=True)
    parser.add_argument('--exclude-reg',  dest='exc_reg',
                        help='Define genome region to excude',  required=True)
    parser.add_argument('--sv-collection',  dest='sv_collection',
                        help='SV collection tar.gz archive, panel of normals',
                        required=True)
    parser.add_argument('--tar-folder',  dest='tar_folder',
                        help=('Path of the SV collection in the '
                              'tar.gz archive. Default '
                              'svCallCollectionsPCAWG'),
                        default='svCallCollectionsPCAWG')
    parser.add_argument('--gc_wig',  dest='ref_gc_wig',
                        help='GC-content wiggle files',
                        required=False)
    parser.add_argument('-x', '--x-heterozygous', dest='female',
                        help=('Flag to set when the X chromomeme '
                              'is heterozygous. eg: set it for '
                              'female genomes'), action='store_true')
    parser.add_argument('--bin',  dest='bin',
                        help=('Number of nt to use for binning'
                              'the final seqz file. Default: 50'),
                        default=200)
    parser.add_argument('--mem',  dest='mem',
                        help=('Amount of max GB of memory to use. '
                              'Default: autodetect'),
                        type=int, required=False)
    parser.add_argument('--ncpu',  dest='ncpu',
                        help='Number of cpu to use. Default: autodetect',
                        type=int, required=False)
    parser.add_argument('--tmp',  dest='tempdir',
                        help='Set the temporary folder',
                        default='/tmp')

    args = parser.parse_args()

    log_dir = os.path.join(os.getcwd(), 'logs')
    log = ExtLog('run_dockstore', log_dir, level=logging.INFO)
    if args.mem:
        log.log.info(('Append PYPE_MEM=%iG to the '
                      'environment variables') % args.mem)
        os.environ['PYPE_MEM'] = '%iG' % args.mem
    if args.ncpu:
        log.log.info(('Append PYPE_NCPU=%i to the '
                      'environment variables') % args.ncpu)
        os.environ['PYPE_NCPU'] = '%i' % args.ncpu

    try:
        tempdir = os.environ['TEMPDIR']
    except KeyError:
        tempdir = args.tempdir

    log.log.info('Prepare temporary diirectory structure')
    prepare_tmp_dirs(tempdir, log, ['databases', 'data', 'workdir'])

    output_dir = '/tmp/workdir'
    results_dir = os.getcwd()

    log.log.info('Output results in folder %s' % output_dir)
    log.log.info('Use profile %s' % 'docker')
    profile = get_profiles({})['docker']

    ref_dict = {'genome_fa_gz': args.ref_gz,
                'gc_10000_bed': args.ref_gc,
                'gencode_excl': args.exc_reg}

    create_symlinks(ref_dict, profile, log)

    sv_collection_path = profile.files['sv_collection']
    log.log.info('Extract sv collection file %s in %s' % (
        args.sv_collection, sv_collection_path))
    unpack_pon_archive(args.sv_collection, sv_collection_path,
                       args.tar_folder)

    setup_ref_files(args.ref_gc_wig, profile, log)

    bam_files = setup_bams(args.tumor_bam, args.normal_bam,
                           args.tumor_bai, args.normal_bai,
                           '/tmp/data/', profile, log)

    # prepare path for processes in the pipeline
    proc_dirs = [os.path.join(output_dir, 'bam_qc'),
                 os.path.join(output_dir, 'delly_combined'),
                 os.path.join(output_dir, 'delly_combined', 'filter_calls'),
                 os.path.join(output_dir, 'plots'),
                 os.path.join(output_dir, 'plots', 'higConf'),
                 os.path.join(output_dir, 'plots', 'lowConf'),
                 os.path.join(output_dir, 'plots', 'circos'),
                 os.path.join(output_dir, 'sequenza'),
                 os.path.join(output_dir, 'seqz')]
    for proc_dir in proc_dirs:
        if not os.path.isdir(proc_dir):
            os.mkdir(proc_dir)

    pype_cmd = ['pype', '--profile', 'docker', 'pipelines',
                '--queue', 'parallel', '--log', log_dir,
                'sv_pipeline_docker',
                '--sample_name', args.run_id,
                '--out_filter', os.path.join(output_dir, 'delly_combined'),
                '--out_vcf', os.path.join(output_dir,
                                          '%s.vcf' % args.run_id),
                '--tumor_bam', bam_files['tumor'],
                '--normal_bam', bam_files['normal'],
                '--normal_cov', os.path.join(
                    output_dir, '%s_normal.cov' % args.run_id),
                '--tumor_cov', os.path.join(output_dir,
                                            '%s_tumor.cov' % args.run_id),
                '--out_cov_plot_high', os.path.join(output_dir, 'plots',
                                                    'higConf'),
                '--out_cov_plot_raw', os.path.join(output_dir, 'plots',
                                                   'lowConf'),
                '--out_circos_plot', os.path.join(
                    output_dir, 'plots', 'circos',
                    '%s_circos.pdf' % args.run_id),
                '--seqz_out', os.path.join(output_dir, 'seqz',
                                           '%s.seqz.gz' % args.run_id),
                '--bin_size', 200,
                '--qc_out',  os.path.join(output_dir, 'bam_qc'),
                '--sequenza_out', os.path.join(output_dir,
                                               'sequenza', args.run_id),
                '--tmp_dir', tempdir]
    if args.female is True:
        pype_cmd += ['--x_heterozygous', 'True']

    pype_cmd = shlex.split(' '.join(map(str, pype_cmd)))
    log.log.info('Prepare pype command line:')
    log.log.info(' '.join(map(str, pype_cmd)))
    pype_proc = subprocess.Popen(pype_cmd)
    pype_proc.communicate()[0]

    proc_time_prefix = os.path.join(output_dir, 'delly_combined', args.run_id)
    proc_time = '%s.runtime.txt' % proc_time_prefix
    runtime_cmd = ['proc_time.py', '-p', log_dir, '-r', args.run_id, '-o', proc_time]
    runtime_cmd = shlex.split(' '.join(map(str, runtime_cmd)))
    log.log.info('Prepare the procces time command line:')
    log.log.info(' '.join(map(str, runtime_cmd)))
    runtime_proc = subprocess.Popen(runtime_cmd)
    out10 = runtime_proc.communicate()[0]

    output_summary = '%s.summary.html' % os.path.join(output_dir, args.run_id)

    raw_file_prefix = os.path.join(output_dir, 'delly_combined', args.run_id)
    raw_file = '%s.bedpe.txt.gz' % raw_file_prefix

    output_noise_filtered_prefix = os.path.join(output_dir, 'delly_combined', args.run_id)
    output_noise_filtered = '%s.noiseFiltered.raw.bedpe.txt.gz' % output_noise_filtered_prefix

    output_highconf_prefix = os.path.join(output_dir, 'delly_combined', 'filter_calls', args.run_id)
    output_highconf = '%s.svFreq.PoN.noiseFiltered.somatic.highConf.bedpe.txt' % output_highconf_prefix

    rmd_input =  os.path.join(output_dir, 'delly.Rmd')
    log.log.info('Use %s template to write %s' % ('/opt/finsen_docs/delly.Rmd', rmd_input))
    with open('/opt/finsen_docs/delly.Rmd', 'rt') as delly_rmd_input:
        delly_rmd_template = delly_rmd_input.read()
        delly_rmd = delly_rmd_template % {'sample_name': args.run_id}
        with open(rmd_input, 'wt') as delly_rmd_out:
            delly_rmd_out.write(delly_rmd)

    files_descr =  os.path.join(output_dir, 'files_description.txt')
    log.log.info('Use %s template to write %s' % ('/opt/finsen_docs/file_names.txt', files_descr))
    with open('/opt/finsen_docs/file_names.txt', 'rt') as file_names_tab:
        file_names_template = file_names_tab.read()
        file_names_sample_id = file_names_template % {'sample_name': args.run_id}
        with open(files_descr, 'wt') as files_descr_out:
            files_descr_out.write(file_names_sample_id)
    if not os.path.exists(output_highconf):
        output_highconf_prefix = os.path.join(output_dir, 'delly_combined', 'filter_calls', args.run_id)
        output_highconf = '%s.svFreq.PoN.somatic.highConf.bedpe.txt' % output_highconf_prefix
    r_code = '''
    library(gtools)
    library(rmarkdown)

    raw.file <- "%(raw.file)s"
    filtered.file <- "%(filtered.file)s"
    PoN.file <- "%(PoN.file)s"
    proc.file <- "%(proc.file)s"
    files.descr <- "%(files.descr)s"
    if(!file.exists(filtered.file)) {
        params <- list(raw = raw.file, descr = files.descr,
            PoNfiltered = PoN.file, proc = proc.file)
    } else {
        params <- list(raw = raw.file, filtered = filtered.file,
            PoNfiltered = PoN.file, proc = proc.file, descr = files.descr)
    }
    rmarkdown::render("%(rmd.input)s", output_file = "%(summary.file)s",
        params = params, envir = new.env(parent = globalenv()))

    ''' % {'rmd.input': rmd_input,
           'files.descr': files_descr,
           'raw.file': raw_file,
           'filtered.file': output_noise_filtered,
           'PoN.file': output_highconf,
           'summary.file': output_summary,
           'proc.file': proc_time}

    log.log.info('Prepare R command line')
    R_cmd = ['R', '--no-save', '--no-environ', '--no-init-file', '--no-restore', '--slave']

    R_cmd = shlex.split(' '.join(map(str, R_cmd)))

    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['R3']))

    log.log.info(' '.join(map(str, R_cmd)))
    log.log.info('Execute R with python subprocess.Popen')
    markdown_proc = subprocess.Popen(R_cmd, stdin=subprocess.PIPE)
    log.log.info('Pipe R code into R command line: %s'
                 % r_code)
    markdown_proc.stdin.write(r_code)
    markdown_proc.stdin.close()
    out0 = markdown_proc.communicate()[0]
    module('rm', program_string(profile.programs['R3']))
    module('rm', 'tools')

    log.log.info('Terminate R markdown')
    # Archive and moving the results

    # prepare output paths for the archiving of the results
    res_dirs = [os.path.join(results_dir, 'bndout'),
                os.path.join(results_dir, 'highconfout'),
                os.path.join(results_dir, 'dellyout'),
                os.path.join(results_dir, 'sequenzaout')]

    for res_dir in res_dirs:
        if not os.path.isdir(res_dir):
            os.mkdir(res_dir)

    bnd_dir = os.path.join(results_dir, 'bndout')
    highconf_dir = os.path.join(results_dir, 'highconfout')
    bamqc_dir = os.path.join(output_dir, 'bam_qc')

    log.log.info('Copy BND results to %s' % bnd_dir)
    filter_dir = os.path.join(
        output_dir, 'delly_combined', 'filter_calls')
    for result in os.listdir(filter_dir):
        filtered_file = os.path.join(filter_dir, result)
        if 'bric_embl-delly' in result:
            copyfile(filtered_file, os.path.join(bnd_dir, result))

    log.log.info('Copy high conf circos plot to %s' % highconf_dir)
    circos_dir = os.path.join(output_dir, 'plots', 'circos')
    for result in os.listdir(circos_dir):
        plot_file = os.path.join(circos_dir, result)
        copyfile(plot_file, os.path.join(highconf_dir, result))

    output_summary_out = os.path.join(highconf_dir,
        os.path.basename(output_summary))
    log.log.info('Copy summary report to %s' % output_summary_out)
    try:
        copyfile(output_summary, output_summary_out)
    except IOError:
        log.log.error('The report file %s was missing' % output_summary_out)
        

    log.log.info('Archive delly_combined results folder %s' %
                 os.path.join(output_dir, 'delly_combined'))
    delly_tar = tarfile.open(os.path.join(
        results_dir, 'dellyout', '%s_delly_combined.tar.gz' % args.run_id),
        'w:gz')
    delly_tar.add(
        os.path.join(output_dir, 'delly_combined'), arcname='delly_combined')
    delly_tar.close()

    cov_tar_file = os.path.join(results_dir, '%s_cov.tar.gz' % args.run_id)
    log.log.info('Create archive for cov files in %s' % cov_tar_file)
    cov_tar = tarfile.open(cov_tar_file, 'w:gz')
    for result in os.listdir(output_dir):
        result = os.path.join(output_dir, result)
        if os.path.isfile(result):
            if result.endswith('.cov.gz') or result.endswith('.cov.log'):
                log.log.info('Add %s to %s archive' % (result, cov_tar_file))
                base_path, file_name = os.path.split(result)
                cov_tar.add(result, arcname=file_name)
    cov_tar.close()

    sqz_dir = os.path.join(output_dir, 'seqz')

    ## Block commentL Do not save sequenza parts archive
    #
    # sqz_part_file = os.path.join(
    #     results_dir, 'sequenzaout', '%s_parts_seqz.tar.gz' % args.run_id)
    # log.log.info(
    #     'Create archive for seqz partial files in %s' % sqz_part_file)
    #
    #
    # sqz_tar = tarfile.open(sqz_part_file, 'w:gz')
    # for result in os.listdir(sqz_dir):
    #    result = os.path.join(sqz_dir, result)
    #    if os.path.isfile(result):
    #        base_path, file_name = os.path.split(result)
    #        if file_name.startswith('%s_part_' % args.run_id):
    #            log.log.info(
    #                'Add %s to %s archive' % (result, sqz_part_file))
    #            sqz_tar.add(result, arcname=file_name)
    # sqz_tar.close()

    sqz_bin_file = os.path.join(
        results_dir, 'sequenzaout', '%s_seqz_bin.tar.gz' % args.run_id)
    log.log.info(('Create archive for seqz binned and '
                  'indexed files in %s') % sqz_bin_file)
    sqz_bin_res_name = '%s_bin%s.seqz.gz' % (args.run_id, args.bin)
    sqz_bin_res = os.path.join(sqz_dir, sqz_bin_res_name)
    sqz_bin = tarfile.open(sqz_bin_file, 'w:gz')
    if os.path.isfile(sqz_bin_res):
        sqz_bin.add(sqz_bin_res, arcname=sqz_bin_res_name)
    else:
        log.log.warning('File %s not found' % sqz_bin_res)
    if os.path.isfile('%s.tbi' % sqz_bin_res):
        sqz_bin.add(
            '%s.tbi' % sqz_bin_res,
            arcname='%s.tbi' % sqz_bin_res_name)
    else:
        log.log.warning('File %s.tbi not found' % sqz_bin_res)
    sqz_bin.close()

    log.log.info('Archive sequenza results folder %s' %
                 os.path.join(output_dir, 'sequenza'))
    sequenza_tar = tarfile.open(os.path.join(
        results_dir, 'sequenzaout', '%s_sequenza.tar.gz' % args.run_id),
        'w:gz')
    sequenza_tar.add(
        os.path.join(output_dir, 'sequenza'), arcname='sequenza')
    sequenza_tar.close()

    log.log.info('Archive log directory %s' % log_dir)
    tar = tarfile.open(
        os.path.join(results_dir, '%s_logs.tar.gz' % args.run_id), 'w:gz')
    tar.add(log_dir, arcname='logs')
    tar.close()

    log.log.info('Archive plots directory %s' %
                 os.path.join(output_dir, 'plots'))
    cov_plots = tarfile.open(os.path.join(
        results_dir, '%s_plots.tar.gz' % args.run_id), 'w:gz')
    cov_plots.add(os.path.join(output_dir, 'plots'), arcname='plots')
    cov_plots.close()

    log.log.info('Archive bam_qc directory %s' % bamqc_dir)
    tar = tarfile.open(
        os.path.join(results_dir, '%s_bam_qc.tar.gz' % args.run_id), 'w:gz')
    tar.add(bamqc_dir, arcname='bam_qc')
    tar.close()


if __name__ == '__main__':
    main()
