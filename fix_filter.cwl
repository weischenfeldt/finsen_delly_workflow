#!/usr/bin/env cwl-runner

class: CommandLineTool
id: fix_delly_bnd
label: fix_delly_bnd
cwlVersion: v1.0

doc: |
    ![build_status](https://img.shields.io/docker/build/weischenfeldt/bric-embl_delly_workflow.svg)
    ![docker_pulls](https://img.shields.io/docker/pulls/weischenfeldt/bric-embl_delly_workflow.svg)
    ![docker_builds](https://img.shields.io/docker/automated/weischenfeldt/bric-embl_delly_workflow.svg)

    ### Fix filter Delly calls

    This workflow takes input from [delly workflow](registry.hub.docker.com/weischenfeldt/bric-embl_delly_workflow) older then 2.0.1_ppcg (included)
    and re-run a fixed version of the last few steps.


dct:creator:
  '@id': http://orcid.org/0000-0003-3684-2659
  foaf:name: Francesco Favero
  foaf:mbox: francesco.favero@bric.ku.dk

dct:contributor:
  foaf:name: Etsehiwot Girum Girma
  foaf:mbox: Etsehiwot@finsenlab.dk

requirements:
  - class: DockerRequirement
    dockerPull: registry.hub.docker.com/weischenfeldt/bric-embl_delly_workflow:2.0.2_ppcg
hints:
  - class: ResourceRequirement
    coresMin: 4
    ramMin: 16384
    outdirMin: 512000
inputs:
  reference-gz:
    type: File
    inputBinding:
      position: 1
      prefix: --reference-gz
  seqz_tar:
    type: File
    inputBinding:
      position: 2
      prefix: --seqz_tar
  delly_filter_tar:
    type: File
    inputBinding:
      position: 3
      prefix: --delly_filter_tar
  cov_tar:
    type: File
    inputBinding:
      position: 4
      prefix: --cov_tar
  mem:
    type: ["null", int]
    inputBinding:
      position: 5
      prefix: --mem
  ncpu:
    type: ["null", int]
    inputBinding:
      position: 6
      prefix: --ncpu
outputs:
  bndout:
    type:
      type: array
      items: File
    outputBinding:
      glob: "bndout/*"
  highconfout:
    type:
      type: array
      items: File
    outputBinding:
      glob: "highconfout/*"
  archives:
    type:
      type: array
      items: File
    outputBinding:
      glob: "*.tar.gz"

baseCommand: [/usr/bin/run_fix_bnd]
